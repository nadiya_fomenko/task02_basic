package com.fomenko;

import java.util.Scanner;

/**
 * task02_basic.
 * Home work:
 * Compile and run java app from console.
 * Write program (Maven project), which will pass requirements:
 * - User enter the interval (for example: [1;100]);
 * - Program prints odd numbers from start to the end of
 * interval and even from end to start;
 * Program prints the sum of odd and even numbers;
 * Program build Fibonacci numbers: F1 will be the biggest
 * odd number and F2 – the biggest even number,
 * user can enter the size of set (N);
 * Program prints percentage of odd and even Fibonacci numbers;
 */

public class Application {

    /**
     * The main method that does all the tasks.
     *
     * @param args default args.
     */

    public static void main(String[] args) {


        System.out.print("Введіть нижню границю інтервалу: ");
        Scanner scan1 = new Scanner(System.in);
        int number1 = scan1.nextInt();

        System.out.print("Введіть верхню границю інтервалу: ");
        Scanner scan2 = new Scanner(System.in);
        int number2 = scan2.nextInt();

        System.out.print("Заданий вами інтервал:  ");
        if (number1 < number2) ;
        else {
            int n = 0;
            n = number2;
            number2 = number1;
            number1 = n;
        }
        System.out.print("[" + "" + number1 + ",");
        System.out.println("" + number2 + "];");


        //print odd numbers
        System.out.println("Перелік непарних чисел: ");
        int i = 0;
        int sumEven = 0;
        for (i = number1; i <= number2; i++) {
            if (i % 2 != 0)
                System.out.print(i + " ");
            sumEven += i;
        }
        System.out.println();

        //prints even numbers
        System.out.println("Реверсний перелік парних чисел: ");
        int k = 0;
        int sumOdd = 0;

        for (k = number2; k >= number1; k--) {
            if (k % 2 == 0) {
                System.out.print(k + " ");
                sumOdd += k;
            }
        }
        System.out.println();
        System.out.println("--------------------------------------");
        System.out.println("Сума непарних чисел = " + sumEven);
        System.out.println("Сума парних чисел = " + sumOdd);


        System.out.println("--------------------------------------");
        System.out.println("Роздрук послідовності Фібоначчі:");
        System.out.println("Введіть кількість членів послідовності: ");
        Scanner scan3 = new Scanner(System.in);
        int N = scan2.nextInt();
        int n0 = 1;
        int n1 = 1;
        int n2 = 0;
        System.out.println("Послідовність:");
        System.out.print(n0 + " " + n1 + " ");
        for (int f = 3; f <= N; f++) {
            n2 = n0 + n1;
            System.out.println(n2 + " ");
            n0 = n1;
            n1 = n2;

        }
        if (n2 % 2 == 0) {
            System.out.println("Найбільше парне число F1: "+ n2);
        } else {
            System.out.println("Найбільше непарне число F2: "+ n2);
        }
        System.out.println();



    }
}
